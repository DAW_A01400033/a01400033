-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 23-03-2018 a las 22:57:20
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Examen`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Registros`
--

CREATE TABLE `Registros` (
  `Hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Entrada` text NOT NULL,
  `Estado` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Registros`
--

INSERT INTO `Registros` (`Hora`, `Entrada`, `Estado`) VALUES
('0000-00-00 00:00:00', '1', 'SYSTEM Failure'),
('0000-00-00 00:00:00', '3', 'SYSTEM Failure'),
('0000-00-00 00:00:00', '3', 'SYSTEM Failure'),
('0000-00-00 00:00:00', 'f', 'SYSTEM Failure'),
('0000-00-00 00:00:00', 'y', 'SYSTEM Failure'),
('0000-00-00 00:00:00', 'y', 'SYSTEM Failure'),
('0000-00-00 00:00:00', 'u', 'SYSTEM Failure'),
('0000-00-00 00:00:00', '8', 'SYSTEM Failure'),
('0000-00-00 00:00:00', '8', 'SYSTEM Failure'),
('0000-00-00 00:00:00', '10', 'SYSTEM Failure'),
('0000-00-00 00:00:00', '4 8 15 16 23 42', 'SUCCESS');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
