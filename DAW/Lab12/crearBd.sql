/*Borrar tabla en caso de que exista y despues crearla */

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')
drop TABLE Materiales

CREATE TABLE Materiales
(
  Clave numeric(5) not null,
  Descripcion varchar(50),
  Costo numeric (8,2)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')
drop TABLE Proveedores
CREATE TABLE Proveedores
(
  RFC char(13) not null,
  RazonSocial varchar(50)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')
drop TABLE Proyectos
CREATE TABLE Proyectos
(
  Numero numeric(5) not null,
  Denominacion varchar(50)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')
drop TABLE Entregan
CREATE TABLE Entregan
(
  Clave numeric(5) not null,
  RFC char(13) not null,
  Numero numeric(5) not null,
  Fecha DateTime not null,
  Cantidad numeric (8,2)
) 

/*Cargar datos */

BULK INSERT a1400033.a1400033.[Materiales]
  FROM 'e:\wwwroot\a1400033\materiales.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

BULK INSERT a1400033.a1400033.[Proyectos]
  FROM 'e:\wwwroot\a1400033\Proyectos.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

BULK INSERT a1400033.a1400033.[Proveedores]
  FROM 'e:\wwwroot\a1400033\Proveedores.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

/*Cambiar formato de fecha*/
SET DATEFORMAT dmy 

BULK INSERT a1400033.a1400033.[Entregan]
  FROM 'e:\wwwroot\a1400033\Entregan.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

/*LLenar una fila de Tabla MAteriales*/
INSERT into Materiales values(1000,'xxx',1000)
/*Elminar registro con dichas caracteristicas*/
DELETE from Materiales WHERE Clave=1000 and Costo=1000
/*Agregar llave primaria (Clave)a Materiales con el nombre llaveMateriales*/
ALTER TABLE Materiales add CONSTRAINT llaveMateriales PRIMARY KEY(Clave)
/*INSERT into Materiales values(1000,'xxx',1000)*/

ALTER TABLE Proveedores add CONSTRAINT llaveProveedores PRIMARY KEY(RFC)
ALTER TABLE Proyectos add CONSTRAINT llaveProyectos PRIMARY KEY(Numero)
ALTER TABLE Entregan add CONSTRAINT llaveEntregan PRIMARY KEY(Clave,RFC,Numero,Fecha)/*Clave(llave primaria de MAteriales), RFC(llave primaria de Proveedores, Numero(llave primaria de Proyectos), fecha(llave primaria Entregan))*/
/*Consulta de constraint de tabla Materiales...Se comenta porque se tiene que correr solo*/
/*sp_helpconstraint Materiales */
/*sp_helpconstraint Proveedores */
/*sp_helpconstraint Proyectos */
/*sp_helpconstraint Entregan */


INSERT INTO Entregan values (0, 'xxx', 0, '1-jan-02', 0) 


Delete from Entregan where Clave = 0 
Select * from Entregan

ALTER TABLE Entregan add constraint cfentreganclave
  foreign key (clave) references Materiales(clave); 

ALTER TABLE Entregan add constraint cfentregannumero
  foreign key (numero) references Proyectos(numero); 

ALTER TABLE Entregan add constraint cfentreganrfc
  foreign key (rfc) references Proveedores(rfc); 

INSERT INTO Entregan values (0, 'xxx', 0, '1-jan-02', 0);
INSERT INTO entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0); 

Delete from Entregan where cantidad = 0
ALTER TABLE Entregan add constraint cantidad check (cantidad > 0) ; 






