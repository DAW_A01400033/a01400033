function getRequestObject() {
    if(window.XMLHttpRequest) {
        return (new XMLHttpRequest());
    }else if(window.ActiveXObject){
        return(new ActiveObject("Microsoft.XMLHTTP"));
    }else{
        return(null);
    }
}

function sendRequest(){
    request=getRequestObject();
    if(request!=null){
        var userInput=document.getElementById('userInput');
        var url='ssajax.php?pattern='+userInput.value;
        request.open('GET',url,true);
        request.onreadystatechange=
                function(){
            if((request.readyState ==4)){
                var ajaxResponse=document.getElementById('ajaxResponse');
                ajaxResponse.innerHTML=request.responseText;
                ajaxResponse.style.visibility="visible";
            }
        };
        request.send(null);
    }
}