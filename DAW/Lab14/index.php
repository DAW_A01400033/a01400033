<!DOCTYPE html>

<html>

    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body>
        <h1>DIMEQYB</h1>
        <h3>Catalogo de medicamentos</h3>
            <table class="table" id="preguntas">
                <?php
                    require_once"util.php";
                    $result= getTableMedicamento();
                    if(mysqli_num_rows($result)>0){
                        echo"<tr>";
                            echo"<td>IDMEDICAMENTO</td>";
                              echo"<td>Nombre</td>";
                              echo"<td>Cantidad en existencia</td>";
                              echo"<td>Precio</td>";
                            echo"</tr>";
                        while($row=mysqli_fetch_assoc($result)){
                            echo"<tr>";
                            echo"<td>". $row["idMedicamento"]. "</td>";
                              echo"<td>". $row["nombreMedicamento"]. "</td>";
                              echo"<td>". $row["cantidad"]. "</td>";
                              echo"<td> $". $row["precio"]. "</td>";
                            echo"</tr>";
                        }
                    }



                ?>
            </table>
        <h1>Busqueda por nombre</h1>
        <table class="table" id="preguntas">
                <?php
                    $nombre="Geno";
                    echo "Nombre a buscar".$nombre;
                    require_once"util.php";
                    $result= getMedicamentobyName($nombre);
                    if(mysqli_num_rows($result)>0){
                        echo"<tr>";
                            
                              echo"<td>Nombre</td>";
                              echo"<td>Precio</td>";
                            echo"</tr>";
                        while($row=mysqli_fetch_assoc($result)){
                            echo"<tr>";
                           
                              echo"<td>". $row["nombreMedicamento"]. "</td>";
                             
                              echo"<td> $". $row["precio"]. "</td>";
                            echo"</tr>";
                        }
                    }



                ?>
            </table>
        
        <table class="table" id="preguntas">
            <thead class="thead-dark">
            
            <tr>
                <th scope="col">Pregunta</th>
                <th scope="col">Respuesta</th>
                
            </tr>
            </thead>
             <tr>
                <th scope="col">¿Qué es ODBC y para qué es útil?</th>
                <th scope="col">Es un estándar de acceso a las bases de datos desarrollado por SQL Access Group, sirve para hacer posible el acceder a cualquier dato desde cualquier aplicación, sin importar qué sistema de gestión de bases de datos (DBMS) almacene los datos.</th>
                
            </tr>
             <tr>
                <th scope="col">¿Qué es SQL Injection? </th>
                <th scope="col">is one of the most common web hacking techniques.</th>
                
            </tr>
             <tr>
                <th scope="col">¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection? </th>
                <th scope="col">
                <ul>
                 Primary Defenses:

    <li>1 Use of Prepared Statements (with Parameterized Queries)</li>
    <li>2 Use of Stored Procedures</li>
   <li> 3 White List Input Validation</li>
    <li>4 Escaping All User Supplied Input</li>
                                    </ul>
                    </th>

                
            </tr>
        
        
        
        
        </table>
    </body>
    <footer>
    </footer>



</html>