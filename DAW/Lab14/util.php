<?php

    function conectDB(){
        
        $servername ="localhost";
        $username = "root";
        $password="";
        $dbname="Lab14";
        
        $con= mysqli_connect($servername,$username,$password,$dbname);
        
        //CheckConnection
        
        if(!$con){
            die("Connection failed".mysqli_connect_error());
        }
        
        return $con;
        
    }

    function closeDB($mysql){
        mysqli_close($mysql);
        
    }

    function getTableMedicamento(){
        $con=conectDB();
        $sql="SELECT * FROM MEDICAMENTO";
        $result=mysqli_query($con,$sql);
        closeDB($con);
        return $result;
    }


    function getMedicamentobyName($nombreaBuscar){
        $con=conectDB();
        $sql= "SELECT nombreMedicamento,precio FROM MEDICAMENTO WHERE nombreMedicamento LIKE '%".$nombreaBuscar."%'";
        $result=mysqli_query($con,$sql);
        closeDB($con);
        return $result;
        
        
    }


?>