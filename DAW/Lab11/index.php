<!DOCTYPE html>


<html>
    
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    


    <body>
        
        <?php
            $passErr = $emailErr = $addErr = $edadErr = $stateErr = $zipErr= "";
            $pass = $email = $add  = $state = $zip = "";
            
            $edad = "";
            
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
              if (empty($_POST["email"])) {
                $emailErr = "email is required";
              } else {
                $email = procesa_Datos($_POST["email"]);
              }

              if (empty($_POST["password"])) {
                $passErr = "Password is required";
              } else {
                $pass = procesa_Datos($_POST["password"]);
              }

              if (empty($_POST["address"])) {
                $addErr = "Address is required";
              } else {
                $add = procesa_Datos($_POST["address"]);
              }

              if (empty($_POST["edad"])||$edad!= is_int($edad)) {
                $edadErr = "Edad is required in a valid form";
              } else {
                $edad = procesa_Datos($_POST["edad"]);
              }

              if (empty($_POST["state"])) {
                $stateErr = "State is required";
              } else {
                $state = procesa_Datos($_POST["state"]);
              }
                if (empty($_POST["zip"])||$zip!= is_int($zip)) {
                $zipErr = "Zip is required in a valid form";
              } else {
                $zip = procesa_Datos($_POST["zip"]);
              }
            }
        
        function procesa_Datos($data){
                $data= trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
        
       
        
        
        ?>
        <h1>Forma de inscripcion</h1>
        <p><span class="text-danger">*requisito obligatorio</span></p>
        <form href="preguntas.html" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputEmail4">Email</label>
              <input type="email" name="email"class="form-control" id="inputEmail4" placeholder="Email">
              <span class="text-danger">*<?php echo $emailErr;?></span>
            </div>
            <div class="form-group col-md-6">
              <label for="inputPassword4">Password</label>
              <input type="password" name = "password"class="form-control" id="inputPassword4" placeholder="Password">
              <span class="text-danger">*<?php echo $passErr;?></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputAddress">Address</label>
            <input type="text"name="address" class="form-control" id="inputAddress" placeholder="1234 Main St">
                 <span class="text-danger">*<?php echo $addErr;?></span>
          </div>
          <div class="form-group">
            <label for="inputAddress2">Address 2</label>
            <input type="text" name ="address2" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
          </div>
          <div class="form-row">
            <div class="form-group col-md-2">
              <label for="inputEdad">Edad</label>
              <input type="number" name="edad" class="form-control" id="inputEdad">
              <span class="text-danger">*<?php echo $edadErr;?></span>
            </div>
            <div class="form-group col-md-4">
              <label for="inputState">State</label>
              <select name ="state" id="inputState" class="form-control">
                <span class="text-danger">*<?php echo $stateErr;?></span>
                <option selected>Elegir opcion</option>
                <option>Aguascalientes</option>
                <option>Queretaro</option>
                <option>Jalisco</option>
                <option>Quintana Roo</option>
              </select>
            </div>
            <div class="form-group col-md-2">
              <label for="inputZip">Zip</label>
              <input type="number" name="zip" class="form-control" id="inputZip">
                <span class="text-danger">*<?php echo $zipErr;?></span>
            </div>
          </div>
          <button  type="submit" class="btn btn-primary">Calcular Precio(Actualizar)</button>
        </form>
        
        <?php
        $renta ="";
        function calcula_Precio($edad){
           
            if($edad<21){
                $renta=100+100;
                
            }else if($edad >21 AND $edad < 60){
                $renta=200+100;
            }else{
             $renta=150+100;
            }
            
            return $renta;
        }
        
        
        echo ("Edad: ".$edad);
        echo"<br>";
        echo $zip;
        $renta=calcula_Precio($edad);

        echo ("total de renta: $".$renta." con una base de renta de $100");
        
        
        
        ?>
        
        
        
        
        
    
        
        

    
    </body>
    <footer>
    
    
    
    
    </footer>






</html>