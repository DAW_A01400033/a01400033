<!DOCTYPE html>
<html>

        <head>
           
             <meta charset="UTF-8">
             <title>Login</title>
             <link rel="stylesheet" type = "text/css" href = "CSS/Style1.css">
             <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
        </head>
        <header>
         <div class="container">
         

         </div>

       </header>
        <body>
            <table>
                <tr>
                    <th>¿Por qué es importante hacer un session_unset() y luego un session_destroy()?</th>
                    <th>
                    <ul>
                        <li>La función session_unset() libera todas las variables de sesión actualmente registradas. </li>
                        <li>session_destroy() destruye toda la información asociada con la sesión actual. No destruye ninguna de las variables globales asociadas con la sesión, ni destruye la cookie de sesión. Para volver a utilizar las variables de sesión se debe llamar a session_start(). </li>
                        <li>En conclusion se deben de realizar los 2 comandos para poder liberar por completo la memoria(variables) y asi destruir por completo la sesion</li>
                        
                    </ul>
                        
                    </th>


                </tr>
                <tr>
                    <th>¿Cuál es la diferencia entre una variable de sesión y una cookie?</th>
                    <th>Las cookies son archivos temporales que guardan informacion de la pagina y se guardan en tu ordenador de tal forma que si tu te creas una cookie con una pagina y luego te sales y vuelves a meterte seguira esa informacion. En cambio si utilizas una sesion, la informacion que tienes "guardada" al salir de esa pagina se borra y no la puedes recuperar.
</th>
                </tr>
                <tr>
                    <th>¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de archivos cuando sube una foto con el mismo nombre?
                    
                    </th>
                    <th>File Hash</th>
                </tr>
                <tr>
                    <th>¿Qué es CSRF y cómo puede prevenirse?</th>
                    <th>es un tipo de vulnerabilidad de aplicaciones web en el que la víctima, sin darse cuenta, ejecuta un script en su navegador que permite aprovecharse de la sesión que hubiese iniciado en algún sitio particular. Los ataques CSRF se pueden hacer con pedidos GET o POST. Se puede prevenir utilizando nombres aleatorios para cada campo del formulario</th>
                </tr>
                <tr>
                 <th>Referencia</th>
            <th>https://es.wikihow.com/evitar-ataques-CSRF-(falsificaci%C3%B3n-de-petici%C3%B3n-en-sitios-cruzados)-con-PHP</th>
                </tr>




                </table>

            
                <div class="container">

          <section class="main row">

             <aside class="col-xs-12 col-sm-12  col-lg-12 col-md-12">
               <h3> Sube una Imagen  </h3>
               <div class="container">
                <form  class="form-group" action="uploadImg.php" method="post" enctype="multipart/form-data"> 
                    <input type="file" name="file"><br>
                    <input type="submit" name="enviar" value="Subir Imagen">

                </form>
            </div>
             </aside>
         </section>

</div>
          </body>
        <footer>
            
            <form name = "close" method="post" action="CloseSession.php">
            <div class = "close">
                
               <input type="submit" name="checkout" value="Cerrar Sesion" >
              
            </div>
            
            
            </form>
          
        </footer>
            
</html>
        