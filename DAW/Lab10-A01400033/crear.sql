CREATE TABLE Materiales
(
  Clave numeric(5),
  Descripcion varchar(50),
  Costo numeric(8,2)
)

CREATE TABLE Proveedores(
	RFC char(13),
	RazonSocial varchar(50)
)

CREATE TABLE Proyectos(
	numero NUMERIC(5),
	denominacion varchar(50)

)

CREATE TABLE Entregan(
	fecha DATETIME,
	cantidad NUMERIC(8,2),
	Clave NUMERIC (5),
	RFC CHAR(13) ,
	Numero NUMERIC (5)
)


