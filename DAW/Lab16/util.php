<?php

    function conectDB(){
        
        $servername ="localhost";
        $username = "root";
        $password="";
        $dbname="Lab14";
        
        $con= mysqli_connect($servername,$username,$password,$dbname);
        
        //CheckConnection
        
        if(!$con){
            die("Connection failed".mysqli_connect_error());
        }
        
        return $con;
        
    }

    function closeDB($mysql){
        mysqli_close($mysql);
        
    }

    function getTableMedicamento(){
        $con=conectDB();
        $sql="SELECT * FROM MEDICAMENTO";
        $result=mysqli_query($con,$sql);
        closeDB($con);
        return $result;
    }


    function getMedicamentobyName($nombreaBuscar){
        $con=conectDB();
        $sql= "SELECT nombreMedicamento,precio FROM MEDICAMENTO WHERE nombreMedicamento LIKE '%".$nombreaBuscar."%'";
        $result=mysqli_query($con,$sql);
        closeDB($con);
        return $result;
        
        
    }

    function insertMedicamento($idMedicamento, $nombreMedicamento,$cantidad, $precio){
        $db = conectDB();
        if ($db != NULL) {
            // insert command specification 
       
        $sql='INSERT INTO MEDICAMENTO(idMedicamento,nombreMedicamento,cantidad,precio) VALUES (?,?,?,?) ';
            
             // Preparing the statement 
            if (!($statement = $db->prepare($sql))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params 
            if (!$statement->bind_param( "ssii",$idMedicamento, $nombreMedicamento,$cantidad,$precio)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
             // Executing the statement
             if (!$statement->execute()) {
                die("Execution failed: (" . $statement->errno . ") " . $statement->error);
              } 
             
          
                closeDB($db);
                return true;
    }
        
        return false;
    }

    function borrarMedicamento($nombreaEliminar){
        $db = conectDB();
        $sql = "DELETE FROM MEDICAMENTO WHERE nombreMedicamento ='" .$nombreaEliminar ."'";
        $result = mysqli_query($db,$sql);
        closeDB($db);
        return $result;
    }

    function updateMedicamento($idMedicamento, $nombreMedicamento, $cantidad, $precio){
        $db = conectDB();
        $sql = "UPDATE MEDICAMENTO SET  nombreMedicamento=$nombreMedicamento, cantidad=$cantidad, precio=$precio WHERE idMedicamento = '".$idMedicamento."'";
        $result = mysqli_query($db,$sql);
        closeDB($db);
        return $result;
    }


?>